const express = require('express');
const Comments = require('../models/Comments');

const auth = require('../middleware/auth');
const permit = require('../middleware/permit');

const createRouter = () => {
  const router = express.Router();

  router.get('/', (req, res) => {
    Comments.find()
      .then(comments => res.send(comments))
      .catch(() => res.sendStatus(500));
  });

  router.post('/', auth, (req, res) => {
    const comments = new Comments(req.body);

    comments.save()
      .then(comments => res.send(comments))
      .catch(error => res.status(400).send(error));
  });

  return router;
};

module.exports = createRouter;