const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const moment = require('moment');

const Image = require('../models/Images');

const auth = require('../middleware/auth');
const permit = require('../middleware/permit');

const config = require('../config');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

const createRouter = () => {
  // Product index
  router.get('/', (req, res) => {
      Image.find().populate('user')
      .then(results => res.send(results))
      .catch(() => res.sendStatus(500));
  });


  router.post('/', [auth, upload.single('image')], (req, res) => {
    const postData = req.body;

    if (req.file) {
      postData.image = req.file.filename;
    } else {
      postData.image = null;
    }
    const image = new Image(postData);
    image.dateTime = moment().format('Do MMMM  YYYY, h:mm');


      image.save()
      .then(result => res.send(result))
      .catch(error => res.status(400).send(error));
  });


  router.get('/:id', (req, res) => {
    const id = req.params.id;
    db.collection('images')
      .findOne({_id: new ObjectId(req.params.id)})
      .then(result => {
        if (result) res.send(result);
        else res.sendStatus(404);
      })
      .catch(() => res.sendStatus(500));
  });

  return router;
};

module.exports = createRouter;