const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ProductSchema = new Schema({
  image: {
    type: String, required: true
  },
  likes: {
    type: Array,
    ofObjectId: [Schema.Types.ObjectId],
    ref: 'User'
  },
  description: String,
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  location:{
    type:String,
    default: 'Moscow'
  },
  comments:{
      type: Array,
      ofObjectId: [Schema.Types.ObjectId],
      ref: 'Comments'
  },
  dateTime:{
    type: String,
    required: true
  }
});

const Images = mongoose.model('Images', ProductSchema);

module.exports = Images;